package mataDra.logics.items;

import mataDra.dao.items.ArmorDAO;
import mataDra.entity.items.equip.ArmorEntity;

public class ArmorListLogic extends ItemListLogic<ArmorEntity> {

	@Override
	public ArmorEntity getItem(int index) {
		// TODO 自動生成されたメソッド・スタブ
		ArmorDAO dao = new ArmorDAO();
		return dao.findByIndex(index);
	}

}
