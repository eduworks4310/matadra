package mataDra.entity.images;

public class EnemyImageEntity extends ImageEntity {
    private String imageBottom;
    private String imageTop;
    /**コンストラクタ自動作成
     * @param index
     * @param name
     * @param imageBottom
     * @param imageTop
     */
    public EnemyImageEntity(int index, String name, String imageBottom, String imageTop) {
        super(index, name);
        this.imageBottom = imageBottom;
        this.imageTop = imageTop;
    }
    //ゲッターセッター自動生成
    public String getImageBottom() {
        return imageBottom;
    }
    public void setImageBottom(String imageBottom) {
        this.imageBottom = imageBottom;
    }
    public String getImageTop() {
        return imageTop;
    }
    public void setImageTop(String imageTop) {
        this.imageTop = imageTop;
    }
    //toString自動生成
    @Override
    public String toString() {
        return "EnemyImage [imageBottom=" + imageBottom + ", imageTop=" + imageTop + ", getImageBottom()="
                + getImageBottom() + ", getImageTop()=" + getImageTop() + ", getIndex()=" + getIndex() + ", getName()="
                + getName() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }


}
