package mataDra.main.test;

import java.util.Scanner;

import mataDra.logics.ui.WaitLogic;

/**ゲーム内待ち時間(ウェイトの確認)
 * @author sensiv
 *
 */
public class WaitTest {
	public static void main(String[] args) {
		WaitLogic waitLogic = new WaitLogic();

		//キーボードで値を入力するたびにウェイト処理実行
		while(true){
		System.out.println("数値を入力してください(0〜8)");
		System.out.println("9:終了");
		int index = new Scanner(System.in).nextInt();

		if(index >= 9){
			break;
		}
		//ウェイト表示
		System.out.println("ウェイト設定" + index);
		waitLogic.wait(index);
		System.out.println("ザ・ワールド");
		waitLogic.wait(index);
		System.out.println("そして時は動き出す");
		waitLogic.wait(index);

		}
	}

}
